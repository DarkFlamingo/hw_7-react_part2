import './reset.css';
import './style.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import MainHeader from './main-header/main-header';
import MessageWrapper from './message-wrapper/message-wrapper';
import Footer from './footer/footer';
import EditMessageModal from './edit-message-modal/edit-message-modal';
import React, { useEffect } from 'react';
import { Preloader } from '../common/common';
import { v4 as uuidv4 } from 'uuid';
import { USER_DEFAULT_URL } from '../../common/constants/constants';
import { useSelector, useDispatch } from 'react-redux';
import { chatActionCreator } from '../../store/actions';
import { ARROW_UP_CODE } from '../../common/constants/constants';

const userId = uuidv4();
const me = { id: userId, name: 'User' };

const Chat = ({ url }) => {
  const {
    messages,
    preloader,
    editModal,
    editableMessage,
    lastCreatedMessage,
  } = useSelector((state) => ({
    messages: state.chat.messages,
    preloader: state.chat.preloader,
    editModal: state.chat.editModal,
    editableMessage: state.chat.editableMessage,
    lastCreatedMessage: state.chat.lastCreatedMessage,
  }));

  const dispatch = useDispatch();

  const handeMessageLoad = React.useCallback(
    (messages) => dispatch(chatActionCreator.setMessages(messages)),
    [dispatch]
  );

  const handleMessageAdd = React.useCallback(
    (message) => dispatch(chatActionCreator.addMessage(message)),
    [dispatch]
  );

  const hadleMessageDelete = React.useCallback(
    (id) => dispatch(chatActionCreator.deleteMessage(id)),
    [dispatch]
  );

  const handleMessageUpdate = React.useCallback(
    (message) => dispatch(chatActionCreator.updateMessage(message)),
    [dispatch]
  );

  const handleSetEditableMessage = React.useCallback(
    (message) => dispatch(chatActionCreator.setEditableMessage(message)),
    [dispatch]
  );

  const handleSetLastCreatedMessage = React.useCallback(
    (message) => dispatch(chatActionCreator.setLastCreatedMessage(message)),
    [dispatch]
  );

  const handleTogglePreloader = React.useCallback(
    (preloader) => dispatch(chatActionCreator.setPreloader(preloader)),
    [dispatch]
  );

  const handleToggleEditModal = React.useCallback(
    (editModal) => dispatch(chatActionCreator.setEditModal(editModal)),
    [dispatch]
  );

  useEffect(() => {
    getMessages(url).then((data) => {
      handeMessageLoad(data);
      handleTogglePreloader(false);
    });
  }, [url, handeMessageLoad, handleTogglePreloader]);

  const getMessages = async (url) =>
    fetch(`${url}`).then((res) => {
      return res.json();
    });

  const addMessage = (text) => {
    if (text) {
      const messageToAdd = {
        id: uuidv4(),
        userId: me.id,
        avatar: USER_DEFAULT_URL,
        user: me.name,
        text,
        createdAt: new Date(Date.now()).toISOString(),
        editedAt: '',
      };
      handleMessageAdd(messageToAdd);
      handleSetLastCreatedMessage(messageToAdd);
    }
  };

  const deleteMessage = (id) => {
    hadleMessageDelete(id);
  };

  const updateMessage = (message, text) => {
    const updatedMessage = {
      ...message,
      text: text,
    };
    handleMessageUpdate(updatedMessage);
    handleToggleEditModal(false);
    handleSetEditableMessage(null);
    if (lastCreatedMessage.id === message.id) {
      handleSetLastCreatedMessage(updatedMessage);
    }
  };

  const editMessage = (messsage) => {
    handleToggleEditModal(true);
    handleSetEditableMessage(messsage);
  };

  const handleOnClose = () => {
    handleToggleEditModal(false);
  };

  const handleKeyDown = (event) => {
    if (event.code === ARROW_UP_CODE) {
      if (lastCreatedMessage !== null) {
        handleSetEditableMessage(lastCreatedMessage);
        handleToggleEditModal(true);
      }
    }
  };

  return (
    <div className="chat" tabIndex="0" onKeyDown={handleKeyDown}>
      <MainHeader />
      {preloader ? (
        <Preloader />
      ) : (
        <>
          <MessageWrapper
            messages={messages}
            me={me}
            addMessage={addMessage}
            deleteMessage={deleteMessage}
            editMessage={editMessage}
          />
          {editModal && (
            <EditMessageModal
              show={editModal}
              onClosed={handleOnClose}
              updateMessage={updateMessage}
              editableMessage={editableMessage}
            />
          )}
        </>
      )}
      <Footer />
    </div>
  );
};

export default Chat;
