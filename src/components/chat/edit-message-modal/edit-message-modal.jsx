import './style.css';
import { Button } from '../../common/common';
import { useState } from 'react';

const EditMessageModal = ({
  show,
  onClosed,
  updateMessage,
  editableMessage,
}) => {
  const [text, setText] = useState(editableMessage.text);

  const textChanged = (data) => {
    setText(data);
  };

  const handleSave = () => {
    updateMessage(editableMessage, text);
  };

  return (
    <div
      className={show ? 'edit-message-modal modal-shown' : 'edit-message-modal'}
    >
      <div className="col-3 modal-wrapper">
        <div className="modal-title">Edit Message</div>
        <input
          type="text"
          onChange={(ev) => textChanged(ev.target.value)}
          value={text}
          className="edit-message-input"
        />
        <div className="modal-btn">
          <Button
            className="edit-message-close"
            variant="secondary"
            onClick={onClosed}
          >
            Close
          </Button>
          <Button
            className="edit-message-button"
            variant="primary"
            onClick={handleSave}
          >
            Save Changes
          </Button>
        </div>
      </div>
    </div>
  );
};

export default EditMessageModal;
