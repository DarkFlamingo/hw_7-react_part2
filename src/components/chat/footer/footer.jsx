import './style.css';

const Footer = () => {
  return (
    <div className="footer w-100">
      <div className="container">
        <div className="footer-copyright">
          <span className="footer-copyright__text">2021 ©DarkFlamingo</span>
        </div>
      </div>
    </div>
  );
};

export default Footer;
