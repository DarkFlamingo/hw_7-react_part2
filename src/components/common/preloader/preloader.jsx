import './style.css';
import { Spinner } from 'react-bootstrap';

const Preloader = () => {
  return (
    <div className="preloader">
      <Spinner animation="border" role="status">
      </Spinner>
    </div>
  );
};

export default Preloader;
