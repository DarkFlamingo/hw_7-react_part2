import { createAction } from '@reduxjs/toolkit';

const ActionType = {
  SET_MESSAGES: 'chat/set-messages',
  ADD_MESSAGE: 'chat/add-message',
  DELETE_MESSAGE: 'chat/delete-message',
  UPDATE_MESSAGE: 'chat/update-message',
  SET_PRELOADER: 'chat/set-preloader',
  SET_EDIT_MODAL: 'chat/set-edit-modal',
  SET_EDITABLE_MESSAGE: 'chat/set-editable-message',
  SET_LAST_CREATED_MESSAGE: 'chat/set-last-created-message',
};

const setMessages = createAction(ActionType.SET_MESSAGES, (messages) => ({
  payload: {
    messages,
  },
}));

const addMessage = createAction(ActionType.ADD_MESSAGE, (message) => ({
  payload: {
    message,
  },
}));

const deleteMessage = createAction(ActionType.DELETE_MESSAGE, (id) => ({
  payload: {
    id,
  },
}));

const updateMessage = createAction(ActionType.UPDATE_MESSAGE, (message) => ({
  payload: {
    message,
  },
}));

const setPreloader = createAction(ActionType.SET_PRELOADER, (preloader) => ({
  payload: {
    preloader,
  },
}));

const setEditModal = createAction(ActionType.SET_EDIT_MODAL, (editModal) => ({
  payload: {
    editModal,
  },
}));

const setEditableMessage = createAction(
  ActionType.SET_EDITABLE_MESSAGE,
  (message) => ({
    payload: {
      message,
    },
  })
);

const setLastCreatedMessage = createAction(
  ActionType.SET_LAST_CREATED_MESSAGE,
  (message) => ({
    payload: {
      message,
    },
  })
);

export {
  setMessages,
  addMessage,
  deleteMessage,
  updateMessage,
  setPreloader,
  setEditModal,
  setEditableMessage,
  setLastCreatedMessage,
};
