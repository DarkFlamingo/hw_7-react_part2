import { reducer as chatReducer } from './chat/reducer';
import { combineReducers } from '@reduxjs/toolkit';

const rootReducer = combineReducers({
  chat: chatReducer,
});

export { rootReducer };
